module.exports = {
    content: [
		"./templates/*.html"
    ],
    theme: {
		extend: {
	    	colors: {
				'moxxy-purple': '#cf4aff',
	    	},
		},
    },
    plugins: [
	require('@tailwindcss/typography'),
    ],
}
