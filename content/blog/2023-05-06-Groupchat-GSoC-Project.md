+++
title = "Moxxy's First GSoC Project!"
date = "2023-05-06"
template = "post.html"

aliases = [ "posts/2023-05-06-Groupchat-GSoC-Project.html" ]

[extra]
author = "Ikjot Singh Dhody"
+++

Hello readers!

As we know, Moxxy is an experimental IM for XMPP built using Flutter. While it does have a great set of features as described [here](https://moxxy.org/), it currently lacks support for group chats (or Multi-User-Chats). A great piece of news is that Google has accepted my proposal to add support for multi user chats in Moxxy.

<!-- more -->

To implement multi user chats in Moxxy, the [XEP-0045](https://xmpp.org/extensions/xep-0045.html) standard will be followed and implemented. The project will run in two phases, since the major changes required will be in the [moxxmpp](https://codeberg.org/moxxy/moxxmpp) and [Moxxy](https://codeberg.org/moxxy/moxxy) codebases. First, XEP-0045 support will be added to moxxmpp with all the handlers, events and routines required to cleanly integrate the same with Moxxy. The second phase will be the UI changes in the Moxxy Flutter application, that builds upon the existing, reusable infrastructure.

The usecases planned to be implemented are listed below (subject to changes before the coding period begins):
- Join a room
- Leave a room
- Change availability status
- View member list
- Send/receive a message to/from all occupants
- Send/receive a private message to/from a specific occupant

Some extra usecases are planned in case time permits:
- Change nickname
- Kick an occupant (if user has such a permission)
- Ban an occupant (if user has such a permission)
- Grant/revoke membership (if user has such a permission)

The coding period for this project will begin on May 29. In case of any queries/interest, please join the following - [xmpp:dev@muc.moxxy.org?join](xmpp:dev@muc.moxxy.org?join).