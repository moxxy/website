+++
title = "GSoC report - Moxxy MUC implementation"
date = "2023-08-20"
template = "post.html"

aliases = [ "posts/2023-08-20-Moxxy-MUC-GSoC-Final-Report.html" ]

[extra]
author = "Ikjot Singh Dhody"
+++

# GSoC Final Report: XMPP Standards Foundation - Moxxy: Implement Group Chats

**Project:** Moxxy - Implement Group Chats  
**Organization:** XMPP Standards Foundation  
**Duration:** March 2023 - August 2023  
**Student:** Ikjot Singh Dhody  
**Mentor:** Alexander

## Introduction

This report presents the culmination of my work on the GSoC project "Moxxy: Implement Group Chats" under the mentorship of Alexander at the XMPP Standards Foundation. Over the course of this project, I have been engaged in implementing a group chat feature for the Moxxy application, aiming to enhance its functionality and user experience.

## About Me

### Personal and Social/Contact Details

- **Name:** Ikjot Singh Dhody
- **Jabber ID:** ikjot-2605[at]jabber.fr
- **Email:** ikjotsd[at]gmail.com
- **GitHub:** [ikjot-2605](https://github.com/ikjot-2605)
- **LinkedIn:** [LinkedIn Profile](https://www.linkedin.com/in/ikjotsd/)

### Educational Background

- **University:** National Institute of Technology Karnataka, Surathkal
- **Major:** Computer Science and Engineering
- **Year:** Senior Year
- **Degree:** Bachelor of Technology (B. Tech)

## The Project
Before getting into the details of the actual project - here is a demo screencast for the implementation:

![](moxxy-muc-demo.gif)

I have outlined the features I implemented through the GSoC period below.
### PRs merged
- moxxmpp
    1. [Add support for XEP-0421](https://codeberg.org/moxxy/moxxmpp/pulls/45)
    2. [Implement XEP-0045 support in moxxmpp](https://codeberg.org/moxxy/moxxmpp/pulls/46)
- Moxxy
    1. [Add groupchat support for Moxxy](https://codeberg.org/moxxy/moxxy/pulls/300)
- website (blog)
    1. [Add blog for GSoC MUC project introduction.](https://codeberg.org/moxxy/website/pulls/2)
    2. [Blog for XEP 0045 support in Moxxmpp](https://codeberg.org/moxxy/website/pulls/3)
    3. [ New blog - Join an MUC with Moxxy!](https://codeberg.org/moxxy/website/pulls/4)
    4. [Final GSoC Report Blog](https://codeberg.org/moxxy/website/pulls/5)
### Feature Use-Cases

The group chat feature required changes to the Moxxy codebase, as well as the moxxmpp codebase. Moxxmpp is the data worker that sends and maintains all the request requirements of the Moxxy frontend. These are the list of contributions across both projects:

- Joining rooms and getting disco info for the room.
- Sending messages to a group chat and receiving messages from the group chat.
- Leaving the group chat.
- Displaying a UI for joining a group chat and providing a nickname.
- Handling group chat details and database support.
- Handling group chat errors and basic error handling.
- Adding a GroupchatDetails model.
- Adding UI elements for group chat, such as title and nickname fields.
- Adding documentation to group chat service methods.
- Minor code refactoring and naming enhancements.
- Enhancing enums for conversation types.
- Removing unnecessary translation strings.
- Improving UI and formatting.

Outlined below are commits made across both codebases throughout the GSoC period.

#### Moxxmpp

There were 2 PRs merged for this codebase. 

- XEP-0421 support for occupant ID support.

| Serial Number | Commit Hash    | Description                                      |
|---------------|----------------|--------------------------------------------------|
| 1             | 255d0f88e0     | feat(xep): Use cascading operation to return state |
| 2             | fa11a3a384     | feat(xep): Checked for the occupant-id directly. |
| 3             | ac5bb9e461     | feat(xep): Implement XEP 0421 in Moxxmpp.       |


- XEP-0045 support for MUC support. 

| Serial Number | Commit Hash    | Description                                      |
|---------------|----------------|--------------------------------------------------|
| 1             | b2724aba0c     | Merge branch 'master' into xep_0045             |
| 2             | d3742ea156     | feat(xep): Small fixes - MUC Example.           |
| 3             | 8b00e85167     | feat(xep): Add example for XEP 0045 Moxxmpp.    |
| 4             | 04dfc6d2ac     | feat(xep): Replace DiscoError with StanzaError. |
| 5             | 9e70e802ef     | Merge branch 'master' into xep_0045             |
| 6             | 3ebd9b86ec     | feat(xep): Fix lint issues and use moxlib for result. |
| 7             | a873edb9ec     | feat(xep): Check for null nick before leaveRoom. |
| 8             | e6bd6d05cd     | feat(xep): Remove NOOP cache access.            |
| 9             | b7d53b8f47     | feat(xep): Add docstings for the XEP-0045 routines |
| 10            | 217c3ac236     | feat(xep): Fix cache issue with join/leaveRoom. |
| 11            | 51bca6c25d     | feat(xep): XEP-0045 cache fixes.                |
| 12            | 8728166a4d     | feat(xep): Add cache and roomstate to MUC implementation. |
| 13            | 1f1321b269     | feat(xep): Small fixes - review cycle 1.        |
| 14            | 66195f66fa     | Merge branch 'master' into xep_0045             |
| 15            | 70fdfaf16d     | feat(xep): Fix imports for xep_0045 files.      |
| 16            | cd73f89e63     | feat(xep): Remove duplicate manager string     |
| 17            | 05c41d3185     | feat(xep): Refactor sendMessage to allow groupchat |
| 18            | 64a8de6caa     | feat(xep): Set base for XEP 0045 implementation |
| 19            | 68809469f6     | feat(xep): Add joinRoom, leaveRoom routines.    |
| 20            | 762cf1c77a     | feat(xep): Set base for XEP 0045 implementation |


#### Moxxy

-  Implement MUCs in Moxxy

| Serial Number | Commit Hash    | Description                                      |
|---------------|----------------|--------------------------------------------------|
| 1             | 549e61a168     | feat(all): Fix linter issues.                   |
| 2             | 26bcaccd81     | Merge branch 'master' into feat/groupchat      |
| 3             | df5810a347     | feat(all): Formatting change, lock file update |
| 4             | ef931c566f     | feat(all): Send messages/chat state from Moxxy to MUC. |
| 5             | a3d4883406     | feat(all): Remove unnecessary buttons/options for MUC. |
| 6             | 532f0b1bb2     | feat(all): Formatting fix, and navigation fix. |
| 7             | a98fe0d9f3     | feat(all): Minor fixes - strings, formatting. |
| 8             | a7c3bd507f     | Merge branch 'master' of https://codeberg.org/moxxy/moxxy into feat/groupchat |
| 9             | fba2cf86ae     | feat(all): Minor formatting changes.           |
| 10            | af481bf465     | feat(all): remove unnecessary comparator override. |
| 11            | e6ae8182c2     | feat(all): Organize import in main.dart.       |
| 12            | de7b9adfa6     | feat(all): Fix user flow for joining MUC.      |
| 13            | e4f98bb82f     | feat(all): Add label to nick text field.       |
| 14            | 56d6f97168     | feat(all): Minor changes, fixes.               |
| 15            | b0067f055f     | feat(all): Move away from exception type design. |
| 16            | bd094dfc9a     | feat(all): Remove unnecessary function.        |
| 17            | 7e9d7d6281     | feat(all): Check if Groupchat exists before method. |
| 18            | 2cf781459d     | feat(all): Debug mode and translatable string  |
| 19            | 4ff9e3c81e     | feat(all): Remove title from GroupchatDetails. |
| 20            | e337f1c579     | feat(all): Minor refactors - naming.           |
| 21            | 7c840334e1     | feat(all): Add docs to groupchat service methods. |
| 22            | 06eab1d6f5     | feat(all): Make ConversationType an enhanced enum. |
| 23            | 008e816d70     | feat(all): Rename JoinGroupchatResultEvent.    |
| 24            | 2bbbc164b5     | feat(all): Remove incorrect translations.       |
| 25            | 11f4fd9932     | feat(all): Add title to GroupchatDetails.      |
| 26            | a1451c6fbf     | feat(all): Refactor groupchat to new object in db. |
| 27            | 993da40297     | feat(all): Complete join groupchat flow.       |
| 28            | 09684b1268     | feat(all): Fix fromDatabaseJson for MUC details. |
| 29            | 0abb89cf38     | feat(all): Fix database issues with nick.      |
| 30            | 7880f51b76     | feat(all): Add db support for GroupchatDetails. |
| 31            | f0a79ca0e0     | feat(all): Add GroupchatDetails model.         |
| 32            | 06dcd5491b     | feat(all): Basic groupchat error handling set up. |
| 33            | 3641be4f56     | feat(all): Join room functionality complete.   |
| 34            | 18e28c3bbf     | feat(all): Groupchat service/bloc updated.     |
| 35            | 62e39bf066     | feat(all): Set base for groupchat implementation. |



## Accomplishments

- Was able to produce a working model of groupchats in Moxxy.

- Started the design approach from scratch and pretty much stuck to it till the end gave me confidence in my design capability.

- Fruitful discussions and review cycles were conducted that allowed for a smooth GSoC experience.

## Challenges Faced

- Needed to understand 2 different codebases, and the architecture of the project. This was a pretty interesting, albeit challenging task that took me a while to master.

- Managing the busy schedule with GSoC work, family, college among other things was difficult. Also, managing a job in the latter parts was also challenging but a rewarding experience.

- The complete virtual approach was slightly difficult, and although not practical - it might have been slightly better with inter-GSoC meetings.


## Learnings/Takeaways

- Implemented group chat (Multi-User Chat or MUC) support in the Moxxy project.
- Successfully added features for joining a group chat room, sending messages to the room, and receiving messages from the room.
- Learned to handle various aspects of XMPP communication, including sending and receiving messages, and interacting with group chat services.
- Gained insights into implementing complex functionality in a Flutter application, involving UI components, service classes, and database interactions.
- Collaborated with experienced developers through code reviews to improve code quality and ensure adherence to best practices.
- Acquired knowledge of working with the XMPP protocol, understanding its intricacies and how it relates to group chat interactions.
- Learned to manage and address feedback from code reviewers, making necessary adjustments to improve code readability and functionality.
- Discovered the importance of proper error handling in real-time communication scenarios, such as handling different types of group chat errors.
- Gained experience in integrating new features while maintaining the overall architecture and user experience of the application.
- Understood the complexities of XMPP server behavior, including handling discussion history and preventing message loops.
- Explored techniques for optimizing communication performance, such as preventing redundant message processing.
- Improved skills in project management, including version control practices, managing branches, and resolving merge conflicts.
- Gained familiarity with the process of creating, testing, and merging pull requests in a collaborative development environment.
- Developed a better understanding of Flutter's state management, navigation, and widget composition for creating feature-rich user interfaces.
- Learned to balance feature development with code quality, addressing both functional and non-functional aspects of the implementation.
- Explored advanced Flutter topics, such as handling asynchronous operations, managing state, and integrating with external libraries.
- Gained a deeper understanding of how group chat and real-time communication play a significant role in modern applications.
- Developed the ability to diagnose and troubleshoot issues related to communication and user interface interactions.
- Strengthened communication skills by participating in technical discussions and effectively conveying ideas and solutions.


## Future Work

The future remaining work has been enlisted in this issue by my mentor [here](https://codeberg.org/moxxy/moxxy/issues/315).

This includes a list of improvements, bug fixes and general future requirements that will be needed to make the  groupchat experience pleasurable for the users of Moxxy.

## Conclusion

Participating in GSoC has been an enriching experience. I've learned valuable skills, collaborated with mentors and the open-source community, and contributed to the Moxxy project. I'm grateful to XMPP Standards Foundation for this opportunity and look forward to continuing my journey in the world of open-source software development.

## Acknowledgments

I would like to express my gratitude to my mentor Alexander for their guidance and support throughout this project. His guidance and understanding is what allowed me to pull through and complete the GSoC with a happy mind. I also want to thank the XMPP Standards Foundation community for their encouragement and feedback.
