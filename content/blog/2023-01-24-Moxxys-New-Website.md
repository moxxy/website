+++
title = "Moxxy's New Website"
date = "2023-01-24"
template = "post.html"

aliases = [ "posts/2023-01-24-Moxxys-New-Website.html" ]

[extra]
author = "PapaTutuWawa"
+++

Hello everyone! Welcome on Moxxy's new website. It currently does not contain
much but that may change in the future.

<!-- more -->

The page is built using [Jekyll](https://jekyllrb.com/) and [TailwindCSS](https://tailwindcss.com/). You can find the page's
source [here](https://codeberg.org/moxxy/website).
