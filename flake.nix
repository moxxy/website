{
  description = "The website for Moxxy";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }: flake-utils.lib.eachDefaultSystem (system: let
    pkgs = import nixpkgs {
      inherit system;
    };

    tailwindWithTypography = (pkgs.nodePackages.tailwindcss.overrideAttrs (old: {
      plugins = [
        pkgs.nodePackages."@tailwindcss/typography"
      ];
    }));

  in {
    devShell = pkgs.mkShell {
      buildInputs = with pkgs; [
        tailwindWithTypography
        zola
        imagemagick
      ];
    };
  });
}
