# Moxxy's Website

See the live website at [moxxy.org](https://moxxy.org).

## Development

Developing the site requires [zola](https://www.getzola.org/) and [tailwindcss](https://tailwindcss.com/).

### Tailwind

The website uses tailwindcss for layouting. To use tailwind, install the
[tailwindcss CLI application](https://tailwindcss.com/docs/installation).
The [typography plugin](https://tailwindcss.com/docs/typography-plugin) is
required for building the website.

### Previewing

In order to test changes, assuming jekyll and tailwindcss are set up, ...

1. Run `tailwindcss -c tailwind.config.js -i input.css --watch --output ./static/css/index.css` from the root of the repository.
2. Run `zola serve` from the root of the repository.

This gives you an environment where tailwindcss and zola regenerate their
previews when modifying their files.

### Building

1. `tailwindcss --input ./input.css --output ./static/css/index.css`
2. `zola build`

### Adding a Blog Post

If you want to add a blog post, considert the following:

- If the post should contain media, create a directory in `./content/blog/` with the following naming scheme `YYYY-MM-DD-<Title>`. Inside that directory, create an `index.md` file where you can write the post in Markdown. The media files can then be placed inside that directory and included using `![](file-name.ext)`.
- If the post does not contain media, create a file in `./content/blog/` with the following naming scheme `YYYY-MM-DD-<Title>`. Inside that file you can write the post in Markdown.

Note that the existing posts specify an alias as this site has been migrated from jekyll to zola. It is not required for new posts. 

## License

See [`./LICENSE`](./LICENSE)
